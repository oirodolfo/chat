from flask import Flask, render_template, json
from faker import Factory, Faker
from flask.ext.sqlalchemy import SQLAlchemy
from random import randint
from flask.ext.cors import CORS

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////www/chat/test.db'
CORS(app)

db = SQLAlchemy(app)

# 
# class Chat(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     sender = db.Column(db.Text, unique=False)
#     receiver = db.Column(db.Text, unique=False)
#     message = db.Column(db.Text, unique=False)
#     seen = db.Column(db.Boolean)
#
#     def __init__(self, sender, receiver, message):
#         self.sender = sender
#         self.receiver = receiver
#         self.message = message
#         self.seen = False


@app.route('/')
def index():
    conversation = Chat.query.all()
    return render_template('index.html', conversation=conversation)


@app.route('/friends')
def api():
    # faker = Factory.create()
    api_data = []
    fake = Factory.create('pt_BR')

    for i in range(1, 10):
        fake.seed(i)
        read = fake.boolean()

        result_api = dict(userId=i, name=fake.name(), text=fake.sentence())

        if read:
            result_api.update(unread=True, amountUnread=randint(0, 9))
        else:
            result_api.update(unread=False)

        api_data.insert(i, result_api)

    return json.dumps(api_data)


app.debug = True

if __name__ == '__main__':
    app.run(
        host="chat.dev",
        port=80
    )
